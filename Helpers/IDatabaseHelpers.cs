﻿using InterviewsManagementNET.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestsInterviews.Helpers
{
    public interface IDatabaseHelpers
    {
        public SubmissionContext GetTestContext();
    }
}
