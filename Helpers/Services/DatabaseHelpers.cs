﻿using InterviewsManagementNET.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestsInterviews.Helpers.Services
{
    public class DatabaseHelpers : IDatabaseHelpers
    {
        public SubmissionContext _context;

        public SubmissionContext GetTestContext()
        {
            var configuration = this.GetLocalConfiguration();

            var dbOptions = new DbContextOptionsBuilder<SubmissionContext>()
                .UseSqlServer(configuration.GetConnectionString("InterviewsConnectionTest"))
                .Options;

            _context = new SubmissionContext(dbOptions);

            return _context;
        }

        private IConfigurationRoot GetLocalConfiguration()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, false)
                .Build();

            return config;
        }
    }
}
