﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsInterviews.FakeRepositories
{
    class InterviewRepositoryFake : IInterviewRepo
    {
        private readonly SubmissionContext _context;
        private readonly IHiringRepo _hiringRepository;

        public InterviewRepositoryFake(SubmissionContext context, IHiringRepo hiringRepository)
        {
            _context = context;
            _hiringRepository = hiringRepository;
        }

        public async Task AddInterviewAsync(Interview interview)
        {
            await _context.Interviews.AddAsync(interview);
        }

        public async Task AddInterviewParticipantAsync(InterviewParticipant interviewParticipant)
        {
            await _context.InterviewParticipant.AddAsync(interviewParticipant);
        }

        public async Task<IEnumerable<InterviewParticipant>> GetAllInterviewParticipantsAsync(int id)
        {
            return await _context.InterviewParticipant.ToListAsync();
        }

        public async Task<IEnumerable<InterviewViewModel>> GetAllInterviewsAsync()
        {
            var interviews = await _context.Interviews.Include(h => h.HiringProcess).Include(c => c.HiringProcess.Submission.User)
                   .Include(c => c.HiringProcess.Submission).ToListAsync();

            var interviewsModel = new List<InterviewViewModel>();

            foreach(var interview in interviews)
            {
                var candidate = await _context.Users.FirstOrDefaultAsync(c => c.Id == interview.HiringProcess.Submission.CandidateId);
                var quiz = await _context.Quizzes.FirstOrDefaultAsync(q => q.QuizId == interview.QuizId);
                var hiringProcess = await _context.Hirings.FirstOrDefaultAsync(h => h.InterviewId == interview.InterviewId);
                var submission = await _context.Submissions.FirstOrDefaultAsync(s => s.SubmissionId == hiringProcess.SubmissionId);
                var position = await _context.Positions.FirstOrDefaultAsync(p => p.PositionId == submission.PositionId);
                var createdBy = await _context.Users.FirstOrDefaultAsync(u => u.Id == interview.CreatedById);
                var lastModifiedBy = await _context.Users.FirstOrDefaultAsync(u => u.Id == interview.LastModifiedById);

                interviewsModel.Add(new InterviewViewModel
                {
                    InterviewId = interview.InterviewId,
                    InterviewDate = interview.InterviewDate,
                    CommunicationChannel = interview.CommunicationChannel,
                    CandidateSituation = hiringProcess.CandidateSituation.ToString(),
                    CandidateFirstName = candidate.FirstName,
                    CandidateLastName = candidate.LastName,
                    PositionName = position.PositionName,
                    Submission = interview.HiringProcess.Submission,
                    HiringProcess = interview.HiringProcess,
                    CreatedBy = createdBy,
                    LastModifiedBy = lastModifiedBy,
                    Quiz = quiz
                });
            }

            return interviewsModel;
        }

        public async Task<IEnumerable<Interview>> GetAllInterviewsByDateAsync(DateTime date)
        {
            return await _context.Interviews.Where(i => i.InterviewDate == date).ToListAsync();
        }

        public async Task<IEnumerable<InterviewParticipant>> GetAllInterviewsParticipantsAsync()
        {
            return await _context.InterviewParticipant.ToListAsync();
        }

        public async Task<Interview> GetInterviewByHiringProcessAsync(HiringProcess hiringProcess)
        {
            return await _context.Interviews.FirstOrDefaultAsync(h => h.HiringId == hiringProcess.HiringId);
        }

        public async Task<Interview> GetInterviewByIdAsync(int id)
        {
            return await _context.Interviews.FirstOrDefaultAsync(i => i.InterviewId == id);
        }

        public async Task<InterviewParticipant> GetInterviewParticipantByIdsAsync(int interviewId, string participantId)
        {
            return await _context.InterviewParticipant.Where(ip => ip.InterviewId == interviewId && ip.EmployeeId == participantId).FirstOrDefaultAsync();
        }

        public async Task removeInterviewAsync(Interview interview)
        {
            HiringProcess hiringProcessToBeUpdated = await _hiringRepository.GetHiringProcessById(interview.HiringId);
            hiringProcessToBeUpdated.InterviewId = null;
            _hiringRepository.UpdateHiringProcess(hiringProcessToBeUpdated);

            await _hiringRepository.SaveChangesAsync();
            _context.Interviews.Remove(interview);
        }

        public void removeParticipantFromInterview(InterviewParticipant interviewParticipant)
        {
            _context.InterviewParticipant.Remove(interviewParticipant);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public Task<IEnumerable<Interview>> ShowAllInterviewsAsync()
        {
            throw new NotImplementedException();
        }

        public void UpdateInterview(Interview interview)
        {
            _context.Interviews.Update(interview);
        }
    }
}
