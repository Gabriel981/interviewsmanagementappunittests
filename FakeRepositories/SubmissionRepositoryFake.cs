﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsInterviews.FakeRepositories
{
    public class SubmissionRepositoryFake : ISubmissionRepo
    {
        private readonly SubmissionContext _context;

        public SubmissionRepositoryFake(SubmissionContext context)
        {
            _context = context;
        }

        public async Task AddSubmissionAsync(Submission submission)
        {
            await _context.Submissions.AddAsync(submission);
        }

        public async Task<IEnumerable<User>> GetAllCandidatesAsync()
        {
            var submissions = await _context.Submissions.ToListAsync();

            if (submissions == null) throw new ArgumentNullException(nameof(submissions));

            var usersWithSubmissions = new List<User>();

            foreach(var submission in submissions)
            {
                usersWithSubmissions.Add(submission.User);
            }

            return usersWithSubmissions;
        }

        public async Task<IEnumerable<SubmissionViewModel>> GetAllSubmissionsAsync()
        {
            var submissionsList = await _context.Submissions.Include(s => s.HiringProcess).ToListAsync();

            var userSubmissions = new List<SubmissionViewModel>();
            
            foreach(var s in submissionsList)
            {
                userSubmissions.Add(new SubmissionViewModel
                {
                    SubmissionId = s.SubmissionId,
                    DateOfSubmission = s.DateOfSubmission,
                    Mentions = s.Mentions,
                    CandidateId = s.CandidateId,
                    User = await _context.Users.FirstOrDefaultAsync(c => c.Id == s.CandidateId),
                    PositionId = s.PositionId,
                    AppliedPosition = await _context.Positions.FirstOrDefaultAsync(p => p.PositionId == s.PositionId),
                    Status = s.HiringProcess.CandidateSituation.ToString(),
                    CV = s.CV
                });
            }

            return userSubmissions;       
        }

        public async Task<IEnumerable<Submission>> GetAllSubmissionsOfficialAsync()
        {
            return await _context.Submissions.ToListAsync();
        }

        public async Task<Submission> GetLastSubmission()
        {
            var lastSubmissionId = await _context.Submissions.MaxAsync(s => s.SubmissionId);
            return await _context.Submissions.FirstOrDefaultAsync(s => s.SubmissionId == lastSubmissionId);
        }

        public async Task<Submission> GetSubmissionByCandidateIdAsync(string id)
        {
            return await _context.Submissions.FirstOrDefaultAsync(s => s.CandidateId == id);
        }

        public async Task<Submission> GetSubmissionByIdAsync(int id)
        {
            return await _context.Submissions.FirstOrDefaultAsync(s => s.SubmissionId == id);
        }

        public async Task<SubmissionViewModel> GetSubmissionByIdDetailedAsync(int id)
        {
            return await _context.Submissions.Include(u => u.User).Include(p => p.AppliedPosition).Select(s => new SubmissionViewModel
            {
                SubmissionId = s.SubmissionId,
                DateOfSubmission = s.DateOfSubmission,
                Mentions = s.Mentions,
                AppliedPosition = s.AppliedPosition,
                CV = s.CV,
                User = s.User,
                Status = _context.Hirings.FirstOrDefault(h => h.SubmissionId == s.SubmissionId).CandidateSituation.ToString()
            }).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<SubmissionViewModel>> GetSubmissionsDetailsAsync()
        {
            return await _context.Submissions.Include(s => s.HiringProcess).Include(s => s.AppliedPosition)
                    .Include(s => s.User).Select(s => new SubmissionViewModel
                    {
                        User = _context.Users.FirstOrDefault(c => c.Id == s.CandidateId),
                        AppliedPosition = s.AppliedPosition,
                        Status = s.HiringProcess.CandidateSituation.ToString(),
                        DateOfSubmission = s.DateOfSubmission,
                        Mentions = s.Mentions,
                        SubmissionId = s.SubmissionId,
                        CandidateId = s.CandidateId,
                        PositionId = s.PositionId
                    }).ToListAsync();
        }

        public void RemoveSubmission(Submission submission)
        {
            _context.Submissions.Remove(submission);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void SubmissionUpdate(Submission submission)
        {
            _context.Submissions.Update(submission);
        }
    }
}
