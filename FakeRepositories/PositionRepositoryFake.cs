﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsInterviews
{
    class PositionRepositoryFake : IPositionRepo
    {
        private readonly SubmissionContext _context;

        public PositionRepositoryFake(SubmissionContext context)
        {
            _context = context;
        }

        public async Task AddPositionAsync(Position position)
        {
            await _context.Positions.AddAsync(position);
        }

        public async Task<IEnumerable<Position>> GetAllPositionsAsync()
        {
            return await _context.Positions.ToListAsync();
        }

        public async Task<Position> GetPositionByIdAsync(int Id)
        {
            return await _context.Positions.Where(p => p.PositionId == Id).FirstOrDefaultAsync();
        }

        public async Task<Position> GetPositionByNameAsync(string positionName)
        {
            return await _context.Positions.Where(p => p.PositionName == positionName).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Position>> GetPositionsByDepartmentId(int departmentId)
        {
            return await _context.Positions.Where(p => p.DepartmentId == departmentId).ToListAsync();
        }

        public void PositionUpdateAsync(Position position)
        {
            _context.Positions.Update(position);
        }

        public void RemovePosition(Position position)
        {
            _context.Positions.Remove(position);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        
    }
}
