﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsInterviews.FakeRepositories
{
    public class UserRepositoryFake : IUserRepository
    {
        private readonly SubmissionContext _context;

        public UserRepositoryFake(SubmissionContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UserViewModel>> GetAllUsers()
        {
            return await _context.Users.Select(u => new UserViewModel
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                DateOfBirth = u.DateOfBirth,
                Address = u.Address,
                Email = u.Email,
                Position = _context.Positions.FirstOrDefault(p => p.PositionId == u.PositionId),
                Role = u.Role,
                Mentions = u.Mentions
            }).ToListAsync();
        }

        public async Task<User> GetUserByEmailAddress(string EmailAddress)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == EmailAddress);
        }

        public async Task<User> GetUserById(string Id)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == Id);
        }

        public Task<User> GetUserByName(string Username)
        {
            return _context.Users.FirstOrDefaultAsync(u => u.UserName == Username);
        }

        public User GetUserByRole(string Role)
        {
            return _context.Users.FirstOrDefault(u => u.Role == Role);
        }

        public void RemoveUser(User user)
        {
            _context.Users.Remove(user);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void UpdateUserDetails(User user)
        {
            _context.Users.Update(user);
        }

        public void UpdateUserPosition(User user)
        {
            _context.Users.Update(user);
        }
    }
}
