﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTestsInterviews.FakeRepositories
{
    public class DepartmentRepositoryFake : IDepartmentRepository
    {
        private readonly SubmissionContext _context;

        public DepartmentRepositoryFake(SubmissionContext context)
        {
            _context = context;
        }

        public void AddDepartment(Department department)
        {
            _context.Departments.Add(department);
        }

        public async Task<Department> GetDepartment(string name)
        {
            return await _context.Departments.Where(d => d.DepartmentName == name).FirstOrDefaultAsync();
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            return await _context.Departments.Where(d => d.DepartmentId == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Department>> GetDepartmentsAsync()
        {
            return await _context.Departments.ToListAsync();
        }

        public void RemoveDepartment(Department department)
        {
            _context.Remove(department);
        }

        public async Task<bool> SaveAllChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void UpdateDepartment(Department department)
        {
            _context.Departments.Update(department);
        }
    }
}
