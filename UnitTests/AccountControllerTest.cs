﻿using AutoMapper;
using InterviewsManagementNET.Controllers;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Profiles;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnitTestsInterviews.FakeRepositories;
using UnitTestsInterviews.Helpers;
using UnitTestsInterviews.Helpers.Services;
using Xunit;

namespace UnitTestsInterviews.UnitTests
{
    public class AccountControllerTest
    {
        IDatabaseHelpers _databaseHelpers;
        IUserRepository _userRepository;
        IPositionRepo _positionRepository;
        SubmissionContext _context;
        AuthController _controller;
/*        Mock<UserManager<User>> _userManager;
*/        Mock<IUserStore<User>> _userStore;
        RoleManager<IdentityRole> _roleManager;
        UserManager<User> _userManager;
        Mock<UserManager<User>> _userManagerMock;
        IMailServices _mailServices;
        


        public AccountControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfiles());
            });

            _databaseHelpers = new DatabaseHelpers();

            _context = _databaseHelpers.GetTestContext();

            var mapper = mockMapper.CreateMapper();

            _userRepository = new UserRepositoryFake(_context);

            _positionRepository = new PositionRepositoryFake(_context);

            _userStore = new Mock<IUserStore<User>>();

            var roleStockMock = new Mock<IRoleStore<IdentityRole>>();

            var userManagerStockMock = new Mock<IUserStore<User>>();

            /*_userStore.As<IUserRoleStore<User>>().Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>(), CancellationToken.None)).Returns(Task.FromResult(IdentityResult.Success));
            _userStore.As<IRoleStore<IdentityRole>>().Setup(x => x.CreateAsync(new IdentityRole("Candidate registered"), CancellationToken.None)).Returns(Task.FromResult(IdentityResult.Success));*/

            var passwordManager = _userStore.As<IUserPasswordStore<User>>();

            _roleManager = new RoleManager<IdentityRole>(roleStockMock.Object, null, null, null, null);

/*            _userManager = new Mock<UserManager<User>>(type, null, null, null, null, null, null, null, null); */

            _userManagerMock = new Mock<UserManager<User>>(_userStore.Object, null, null, null, null, null, null, null, null);

            /*            _userManager = new UserManager<User>(_userStore.Object, null, null, null, null, null, null, null, null);
             *            
            */

            _userManagerMock.Setup(x => x.DeleteAsync(It.IsAny<User>())).ReturnsAsync(IdentityResult.Success);
            _userManagerMock.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            var userManagerFinal = _userManagerMock.Object;
            
            _mailServices = new MailServicesImpl();

            _controller = new AuthController(userManagerFinal, _userRepository, null, null, _positionRepository, _roleManager, null, null, null, null, _mailServices) ;
        }

        [Fact]
        public async Task Should_Add_UserAsync()
        {
            User user = new User()
            {
                FirstName = "Candidate",
                LastName = "Candidate 1",
                Address = "Address",
                Email = "candidate.candidate1@mail.com",
                DateOfBirth = DateTime.Now,
                SecurityQuestion = "security question 1",
                SecurityQuestionAnswer = "security answer 1"
            };

            var result = await _controller.RegisterCandidate(new UserRegisterViewModel()
            {
                FirstName = "Candidate",
                LastName = "Candidate 1",
                Address = "Address",
                EmailAddress = "candidate.candidate1@mail.com",
                SecurityQuestion = "security question",
                SecurityQuestionAnswer = "security question answer"
            });
            
        }

    }
}
