﻿using AutoMapper;
using InterviewsManagementNET.Controllers;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Profiles;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnitTestsInterviews.FakeRepositories;
using UnitTestsInterviews.Helpers;
using UnitTestsInterviews.Helpers.Services;
using Xunit;

namespace UnitTestsInterviews.UnitTests
{
    public class HiringProcessControllerTest
    {
        IHiringRepo _repository;
        IDatabaseHelpers _databaseHelpers;
        SubmissionContext _context;
        HiringProcessController _controller;

        public HiringProcessControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfiles());
            });

            var mapper = mockMapper.CreateMapper();
            _databaseHelpers = new DatabaseHelpers();

            _context = _databaseHelpers.GetTestContext();
            _context.Database.EnsureCreated();


            _repository = new HiringRepositoryFake(_context);

            _controller = new HiringProcessController(_repository, mapper);
        }

        [Fact]
        public async Task GetHiringValues_NonEmpty_WhenCalledAsync()
        {
            var hiringProcesses = await _controller.GetAllHiringProcessesAsync();
            var okResult = hiringProcesses as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var modelList = okResult.Value as IEnumerable<HiringViewModel>;

            var assertionModelList = Assert.IsType<List<HiringViewModel>>(modelList);

            var actual = assertionModelList.Count;

            Assert.True(actual >= 1);
        }

        [Fact]
        public async Task GetHiringProcess_ByIdProvided_WhenCalledAsync()
        {
            int actualId_called = 1026;
            var hiringProcess = await _controller.GetHiringProcessByIdAsync(actualId_called);
            var okResult = hiringProcess as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var model = okResult.Value as HiringViewModel;

            var assertionModelType = Assert.IsType<HiringViewModel>(model);

            var expectedId = 1026;
            var actualId = assertionModelType.HiringId;

            Assert.Equal(expected: expectedId, actual: actualId);
        }
    }
}
