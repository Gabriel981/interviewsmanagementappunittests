﻿using AutoMapper;
using InterviewsManagementNET.Controllers;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Profiles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UnitTestsInterviews.FakeRepositories;
using UnitTestsInterviews.Helpers;
using Xunit;

namespace UnitTestsInterviews
{
    public class PositionControllerTest
    {
        PositionsController _controller;
        IPositionRepo _repository;
        IDepartmentRepository _departmentRepository;
        SubmissionContext _context;

        public PositionControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfiles());
            });

            var mapper = mockMapper.CreateMapper();

            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, false)
                .Build();


            var dbOptions = new DbContextOptionsBuilder<SubmissionContext>()
              .UseSqlServer(config.GetConnectionString("InterviewsConnectionTest"))
              .Options;

            var _context = new SubmissionContext(dbOptions);

            _context.Database.EnsureCreated();

            _repository = new PositionRepositoryFake(_context);
            _departmentRepository = new DepartmentRepositoryFake(_context);
            _controller = new PositionsController(_repository, mapper, _context, _departmentRepository, null);
        }

        [Fact]
        public async Task Get_OkResult_WhenCalledAsync()
        {
            //Act
            var positionsReturned = await _controller.GetAllPositionsAsync();
            var okResult = positionsReturned.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            //Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact, TestPriority(5)]
        public async Task Get_AllPositions_WhenCalledAsync()
        {
            //Act
            var positions = await _controller.GetAllPositionsAsync();
            var okResult = positions.Result as OkObjectResult;
            if(okResult != null)
            {
                Assert.NotNull(okResult);
            }

            var model = okResult.Value as IEnumerable<PositionReadDTO>;

            var assertionTypeResult = Assert.IsType<List<PositionReadDTO>>(model);


            if(assertionTypeResult.Count() > 0)
            {
                Assert.NotNull(assertionTypeResult);

                var expected = 3;
                var actual = assertionTypeResult.Count();

                Assert.Equal(expected: expected, actual: actual);
            }
        }

        [Fact, TestPriority(4)]
        public async Task GetOkResultWhenPositionIdRequested_WhenCalledAsync()
        {
            var requestedId = 1;
            var position =  await _controller.GetPositionByIdAsync(requestedId);

            var okResult = position.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            Assert.IsType<OkObjectResult>(okResult);
        }
    
        [Fact, TestPriority(3)]
        public async Task GetPositionId_WhenCalledAsync()
        {
            var requestedId = 1;
            var position = await _controller.GetPositionByIdAsync(requestedId);

            var okResult = position.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var assertionTypeTest = Assert.IsType<PositionReadDTO>(okResult.Value as PositionReadDTO);

            var expected = new Position() { PositionId = 1, PositionName = "Position 1", DepartmentId = 1, AvailableForRecruting = true, Description = "Description 1", NoVacantPositions = 1 };

            Assert.Equal(expected: expected.PositionId, actual: assertionTypeTest.PositionId);
        }
    
        [Fact, TestPriority(1)]
        public async Task AddNewPosition_WhenRequestedAsync()
        {
            PositionWriteDTO positionTest = new PositionWriteDTO()
            {
                PositionName = "Position 4",
                DepartmentId = 1,
                Description = "Description 4",
                NoVacantPositions = 1
            };

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] {
                new Claim(ClaimTypes.Name, "Georgel"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email, "georgel.georgel@mail.com"),
            }, "mock"));

            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };          

            await _controller.AddPositionAsync(positionTest);

            int testId = 1;

            var positionAwaiter = await _controller.GetPositionByIdAsync(testId);

            var okResult = positionAwaiter.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var result = Assert.IsType<PositionReadDTO>(okResult.Value as PositionReadDTO);

            var expectedResultId = 1;
            var actualModel = result.PositionId;

            Assert.Equal(expected: expectedResultId, actual: actualModel);
        }

        [Fact, TestPriority(6)]
        public async Task RemovePosition_WhenCalled()
        {
            var positionId = 1;
            var position = await _controller.GetPositionByIdAsync(positionId);
            var okObject = position.Result as OkObjectResult;
            if (okObject != null) Assert.NotNull(okObject);

            var result = Assert.IsType<PositionReadDTO>(okObject.Value as PositionReadDTO);

            await _controller.RemovePositionAsync(result.PositionId);

            var positionAfterDelete = await _controller.GetPositionByIdAsync(positionId);
            var notFoundObject = positionAfterDelete.Result as OkObjectResult;

            var valueReturned = notFoundObject.Value as PositionReadDTO;

            Assert.True(valueReturned == null);
        } 

    }



}
