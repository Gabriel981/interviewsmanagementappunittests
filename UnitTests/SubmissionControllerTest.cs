﻿using AutoMapper;
using InterviewsManagementNET.Controllers;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Profiles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnitTestsInterviews.FakeRepositories;
using UnitTestsInterviews.Helpers;
using UnitTestsInterviews.Helpers.Services;
using Xunit;

namespace UnitTestsInterviews.UnitTests
{
    public class SubmissionControllerTest
    {
        IDatabaseHelpers _databaseHelper;
        MoqHelper _moqHelper;

        SubmissionsController _mainController;
        PositionsController _positionController;

        ISubmissionRepo _submissionRepository;
        IHiringRepo _hiringRepository;
        IPositionRepo _positionRepository;
        IUserRepository _userRepository;
        IInterviewRepo _interviewRepository;
        SubmissionContext _databaseContext;

        Mock<IUserStore<User>> _userStore;
        Mock<UserManager<User>> _userManager;


        //Optional data
        private List<User> _users = new List<User>
        {
            new User() {FirstName = "Candidate 1", LastName = "Candidate", Address = "Address", Email = "candidate.candidate1@mail.com", PhoneNumber = "072928282828", DateOfBirth = DateTime.Now, SecurityQuestion = "security question", SecurityQuestionAnswer = "security question answer"},
            new User() {FirstName = "Candidate 2", LastName = "Candidate", Address = "Address", Email = "candidate.candidate2@mail.com", PhoneNumber = "072928282829", DateOfBirth = DateTime.Now, SecurityQuestion = "security question", SecurityQuestionAnswer = "security question answer"}
        };

        public SubmissionControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfiles());
            });

            _databaseHelper = new DatabaseHelpers();

            _databaseContext = _databaseHelper.GetTestContext();

            var mapper = mockMapper.CreateMapper();

            _submissionRepository = new SubmissionRepositoryFake(_databaseContext);
            _hiringRepository = new HiringRepositoryFake(_databaseContext);
            _positionRepository = new PositionRepositoryFake(_databaseContext);
            _userRepository = new UserRepositoryFake(_databaseContext);
            _interviewRepository = new InterviewRepositoryFake(_databaseContext, _hiringRepository);
            _mainController = new SubmissionsController(_submissionRepository, mapper, _hiringRepository, _positionRepository, _userRepository, _interviewRepository);

            _moqHelper = new MoqHelper();

            _userStore = new Mock<IUserStore<User>>();

            _userManager = new Mock<UserManager<User>>(_userStore.Object, null, null, null, null, null, null, null, null);
        }

        [Fact]
        public async Task Should_Return_OkResponse()
        {
            var retrievedSubmissions = await _mainController.GetAllSubmissionsAsync();
            var okResult = retrievedSubmissions.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            Assert.IsType<List<SubmissionViewModel>>(okResult.Value);


            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);

            Assert.True(okResult is OkObjectResult);
        }
       
        [Fact]
        public async Task Should_Return_All_Submissions()
        {
            var retrievedSubmissions = await _mainController.GetAllSubmissionsAsync();
            var okResult = retrievedSubmissions.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var result = Assert.IsType<List<SubmissionViewModel>>(okResult.Value);

            var actualCount = result.Count;
            var expectedResult = 10;

            Assert.Equal(expected: expectedResult, actual: actualCount);
        }

        [Fact]
        public async Task Should_return_no_object()
        {
            var retrievedSubmissions = await _mainController.GetAllSubmissionsAsync();
            var okResult = retrievedSubmissions.Result as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var result = Assert.IsType<List<SubmissionViewModel>>(okResult.Value);

            var actualCount = result.Count;
            var expectedCount = 0;

            Assert.Equal(expected: expectedCount, actual: actualCount);
        }

        [Fact]
        public async Task Should_add_single_submission()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] {
                new Claim(ClaimTypes.Name, "Georgel"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Email, "georgel.georgel@mail.com"),
            }, "mock"));

            _mainController.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            SubmissionWriteDTO submissionWrite = new SubmissionWriteDTO()
            {
                FirstName = "Ionel",
                LastName = "Palade",
                EmailAddress = "ionel.palade@mail.com",
                PositionId = 1
            };

            User newUser = new User()
            {
                FirstName = "Candidate",
                LastName = "Candidate 1",
                Address = "Address",
                DateOfBirth = DateTime.Now,
                Email = "candidate.candidate1@mail.com",
                PhoneNumber = "078282828"
            };

            _userStore.Setup(x => x.CreateAsync(newUser, CancellationToken.None))
                .Returns(Task.FromResult(IdentityResult.Success));

            _userStore.Setup(x => x.FindByNameAsync(newUser.Email, CancellationToken.None))
                .Returns(Task.FromResult(newUser));

            _userManager.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            Task<IdentityResult> identityResult = _userStore.Object.CreateAsync(newUser, CancellationToken.None);
            Task<IdentityResult> identityResultUser = _userManager.Object.CreateAsync(newUser);

            if (identityResult.Result.Succeeded)
            {
                var result = await _mainController.UploadSubmissionAsync(submissionWrite);
                var okOjbectRes = result.Result as OkObjectResult;
                if (okOjbectRes != null) Assert.NotNull(okOjbectRes);

                Assert.IsType<string>(okOjbectRes.Value);
            }
        }

    }
}
