﻿using AutoMapper;
using InterviewsManagementNET.Controllers.AdministrativeControllers;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO.DepartmentDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Profiles;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestsInterviews.FakeRepositories;
using UnitTestsInterviews.Helpers;
using UnitTestsInterviews.Helpers.Services;
using Xunit;

namespace UnitTestsInterviews.UnitTests
{
    [TestCaseOrderer("TestOrderExamples.TestCaseOrdering.PriorityOrderer", "TestOrderExamples")]
    public class DepartmentControllerTest
    {
        IDatabaseHelpers _helpers;
        DepartmentController _controller;
        IDepartmentRepository _repository;
        IPositionRepo _positionRepository;
        SubmissionContext _context;

        public DepartmentControllerTest()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfiles());
            });

            var mapper = mockMapper.CreateMapper();

            _helpers = new DatabaseHelpers();

            _context = _helpers.GetTestContext();
            _context.Database.EnsureCreated();

            _repository = new DepartmentRepositoryFake(_context);
            _positionRepository = new PositionRepositoryFake(_context);

            _controller = new DepartmentController(_repository, _positionRepository, mapper);
        }

 /*       [Fact, TestPriority(1)]
        public async Task GetEmptyDatabase_WhenCalledAsync()
        {
            var departmentsReturned = await _controller.GetDepartmentsAsync();
            var okResult = departmentsReturned as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var model = okResult.Value as IEnumerable<DepartmentReadDTO>;

            var assertionTypeResult = Assert.IsType<List<DepartmentReadDTO>>(model);

            var expected = 0;
            var actual = assertionTypeResult.Count();

            Assert.Equal(expected: expected, actual: actual);
        }*/

        [Fact, TestPriority(4)]
        public async Task GetDepartmentsValues_NonEmpty_WhenCalledAsync()
        {
            var departmentsReturned = await _controller.GetDepartmentsAsync();
            var okResult = departmentsReturned as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var model = okResult.Value as IEnumerable<DepartmentReadDTO>;

            var assertionTypeResult = Assert.IsType<List<DepartmentReadDTO>>(model);

            var expected = 30;
            var actual = assertionTypeResult.Count();

            Assert.Equal(expected: expected, actual: actual);
        }

        [Fact, TestPriority(1)]
        public async Task AddDepartmentTest()
        {
            DepartmentWriteDTO testDepartment = new DepartmentWriteDTO() { DepartmentName = "Department 1", Active = true, Description = "Description", CurrentNumberOfPositions = 0 };

            var okResult = await _controller.AddDepartmentAsync(testDepartment) as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);

            var departmentsReturned = await _controller.GetDepartmentByName(testDepartment.DepartmentName);
            var okResult2 = departmentsReturned as OkObjectResult;

            if (okResult2 != null) Assert.NotNull(okResult2);
            var result = Assert.IsType<OkObjectResult>(okResult2);


            var expected = "Department 1";
            var actual = result.Value as DepartmentReadDTO;

            Assert.Equal(expected: expected, actual: actual.DepartmentName);
        }

        [Fact, TestPriority(2)]
        public async Task AddMultipleDepartmentsTest()
        {
            List<DepartmentWriteDTO> departments = new List<DepartmentWriteDTO>();

            for (int i = 1; i < 31; i++)
            {
                departments.Add(new DepartmentWriteDTO()
                {
                    DepartmentName = "Department " + i,
                    Description = "Description",
                    Active = true,
                    CurrentNumberOfPositions = i
                });
            }

            foreach(DepartmentWriteDTO departmentWrite in departments)
            {
                var okResult = await _controller.AddDepartmentAsync(departmentWrite);
                if (okResult != null) Assert.NotNull(okResult);
            }

            var departmentsRetrieved = await _controller.GetDepartmentsAsync();
            var okResult2 = departmentsRetrieved as OkObjectResult;

            if (okResult2 != null) Assert.NotNull(okResult2);
            var results = Assert.IsType<OkObjectResult>(okResult2);

            var expectedCount = 30;
            var actualCount = results.Value as IEnumerable<DepartmentReadDTO>;

            Assert.Equal(expected: expectedCount, actual: actualCount.Count());
        }

        [Fact, TestPriority(5)]
        public async Task RemoveDepartment_WhenCalled()
        {
            var departmentIdTest = 1;

            var model = await _controller.RemoveDepartmentAsync(departmentIdTest);
            var okResultDelete = model as OkObjectResult;
            if (okResultDelete != null) Assert.NotNull(okResultDelete);


            var departmentF = await _controller.GetDepartmentByIdAsync(departmentIdTest);
            var okResultDepartment = departmentF as OkObjectResult;

            Assert.True(okResultDepartment == null);
        }

        [Fact, TestPriority(3)]
        public async Task UpdateDepartmentDetails_WhenCalled()
        {
            string newDepartmentName = "Department x";
            int departmentIdTest = 1;
            
            var departmentFound = await _controller.GetDepartmentByIdAsync(departmentIdTest);
            var okResult = departmentFound as OkObjectResult;
            if (okResult != null) Assert.NotNull(okResult);
            var resultDepartment = Assert.IsType<DepartmentReadDTO>(okResult.Value);

            var departmentModel = resultDepartment;
            DepartmentUpdateDTO departmentUpdateDTO = new DepartmentUpdateDTO()
            {
                DepartmentName = newDepartmentName,
                Active = departmentModel.Active,
                Description = departmentModel.Description
            };

            var result = await _controller.UpdateDepartmentAsync(departmentIdTest, departmentUpdateDTO);
            var okUpdateResult = result as OkObjectResult;
            if (okUpdateResult != null) Assert.NotNull(okUpdateResult);

            var departmentUpdated = await _controller.GetDepartmentByIdAsync(departmentIdTest);
            var okResultRetrieval = departmentUpdated as OkObjectResult;

            var resultActual = Assert.IsType<DepartmentReadDTO>(okResultRetrieval.Value);

            var expectedName = "Department x";
            var actualName = resultActual.DepartmentName;

            Assert.Equal(expected: expectedName, actual: actualName);
        }
    }
}
